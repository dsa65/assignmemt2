import ballerinax/kafka;
// import ballerina/log;
import ballerinax/mongodb;
import ballerina/io;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "group-receive",
    // Subscribes to the topic `test-kafka-topic`.
    topics: ["order"],

    pollingInterval: 1
};

public type Shipping record {|
    string city;
    string street;
    int erf;
|};

public type Item record {
    string sku;
    int quantity;
    int store = 0;
};

public type Order record {|
    int userId = 0;
    int orderId;
    boolean isValid;
    Shipping shipTo;
    Item[] items;
|};

public type ShippingOrder record {
    record {
        int store;
        int quantity;
    }[] collection = [];
    int total = 0;
    string sku = "";
};

public type ProcessedRes record {
    boolean fulfilled;
    ShippingOrder currentOrder;
};

public type SuccessfulOrder record {|
    int userId = 0;
    int orderId;
    boolean isValid;
    Shipping shipTo;
    ShippingOrder[] items;
|};

// Create a subtype of `kafka:AnydataConsumerRecord`.
public type OrderConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    Order value;
|};

public type OrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    Order value;
|};

public type SuccessfulOrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    SuccessfulOrder value;
|};

mongodb:ConnectionConfig mongoConfig = {
    options: {url: "mongodb+srv://mario:<kalimba>@cluster0.jktjt8h.mongodb.net/test"}
};
string database = "order";
mongodb:Client mongoClient = check new (mongoConfig, database);

kafka:Producer producer = check new (kafka:DEFAULT_URL);

service on new kafka:Listener(kafka:DEFAULT_URL, consumerConfigs) {
    remote function onConsumerRecord(OrderConsumerRecord[] records) returns error? {
        // The set of Kafka records received by the service are processed one by one.
        boolean orderFulfilled = false;
        ShippingOrder[] allItems = [];

        check from OrderConsumerRecord orderRecord in records
            do {
                check from Item item in orderRecord.value.items
                    do {
                        stream<Item, error?> result = check mongoClient->find("catalogue", filter = {sku: item.sku});

                        ProcessedRes|error? res = check processOrder(result, item);
                        if (res is ProcessedRes) {
                            if (res.fulfilled) {
                                allItems.push(res.currentOrder);
                                orderFulfilled = true;
                            } else {
                                orderFulfilled = false;
                                return;
                            }
                        }
                    };

                // order could not be fulfilled produce data stating that
                if (!orderFulfilled) {
                    io:println("+++++++++++++++++++++++++++++ cannot be fulfilled");
                    OrderProducerRecord producerRecord = {
                    topic: "unfulfilledOrder",
                    value: orderRecord.value
                    };

                    // Sends the message to the Kafka topic.
                    check producer->send(producerRecord);
                } else {
                    // deduct quantities from the db
                    foreach ShippingOrder current in allItems {
                        foreach var currentJ in current.collection {
                            map<json> replaceFilter = {sku: current.sku, store: currentJ.store};
                            map<json> replaceDoc = {"$inc": {quantity: -currentJ.quantity}};
                            int modifiedCount = check mongoClient->update(replaceDoc, "catalogue", (), replaceFilter, true);

                            io:println("Updated records: ", modifiedCount);
                            io:println("SKU: ", current.sku);
                            io:println("Store: ", currentJ.store);

                        }

                    }

                    // produce message to move to the next step
                    SuccessfulOrderProducerRecord producerRecord = {
                    topic: "processedOrder",
                    value: {
                        orderId: orderRecord.value.orderId,
                        isValid: true,
                        shipTo: orderRecord.value.shipTo,
                        items: allItems
                    }};

                    // Sends the message to the Kafka topic.
                    check producer->send(producerRecord);
                }
            };

        // mongoClient->close();
    }
}

public function processOrder(stream<Item, error?> result, Item item) returns ProcessedRes|error? {
    ShippingOrder tmp = {total: 0, sku: item.sku};

    check from Item data in result
        do {
            if (tmp.total != item.quantity) {
                int needed = item.quantity - tmp.total;
                int recieved = data.quantity < needed ? data.quantity : needed;
                tmp.collection.push({store: data.store, quantity: recieved});
                tmp.total += recieved;
            }
        };

    return {currentOrder: tmp, fulfilled: item.quantity == tmp.total};
}
